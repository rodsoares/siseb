<?php

namespace Venus\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Venus\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        return view ('admin.dashboard');
    }
}
