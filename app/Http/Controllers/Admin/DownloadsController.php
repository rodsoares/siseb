<?php

namespace Venus\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Venus\Http\Controllers\Controller;
use Venus\Models\Product as Ebook;

class DownloadsController extends Controller
{
    public function download( $slug )
    {
        $ebook = Ebook::where('slug', $slug)->first();
        return response()->download(storage_path('app/'.$ebook->file));
    }
}
