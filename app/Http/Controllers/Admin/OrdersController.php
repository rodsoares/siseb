<?php

namespace Venus\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Venus\Http\Controllers\Controller;
use Venus\Models\Order;
use Venus\Models\OrderedProduct;
use Venus\Models\Product;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$products = Product::where('status', 1)->get();
        $orders   = null;

        if ( in_array('admin', Auth::user()->getRoles()) )
        {
            $orders = Order::all();
        } else {
            $orders = Order::where('user_id', Auth::user()->id);
        }

        return view('admin.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::where('status', 1)->get();
        return view('admin.orders.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = Order::create( $request->input() );
        $ids = $request->input('ebooks');

        foreach( $ids as $id )
        {
            $op = new OrderedProduct();
            $op->order_id = $order->id;
            $op->product_id = $id;
            $op->save();
        }

        /** Enviar email aqui */

        return redirect()->route('admin.oders.index')->with(['success' => 'Pedido realizado com sucesso']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
