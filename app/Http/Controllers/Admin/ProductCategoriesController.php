<?php

namespace Venus\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Venus\Http\Controllers\Controller;
use Venus\Http\Requests\ProductCategory\StoreProductCategoryRequest;
use Venus\Models\ProductCategory as Category;

class ProductCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductCategoryRequest $request)
    {
        try {
            $category = Category::create( $request->all() );
            return redirect()->route('product-categories.index')->with([
                'success' => 'Categoria adicionada com sucesso.'
            ]);
        } catch (\Exception $e) {
            return redirect()->route('product-categories.create')->with([
                'error' => 'Ocorreu um erro ao processar requisição. Tente novamente em alguns instantes.'
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);

        $category->fill( $request->all() );

        if ($category->save())
        {
            return redirect()->route('product-categories.index')->with([
                'success' => 'Categoria atualizada com sucesso.'
            ]);
        } else {
            return redirect()->route('product-categories.create')->with([
                'error' => 'Ocorreu um erro ao processar requisição. Tente novamente em alguns instantes.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);

        if ($category->delete() )
        {
            return redirect()->route('product-categories.index')->with(['success' => 'Categoria deletado com sucess']);
        } else {
            return redirect()->route('product-categories.index')->with(['error' => 'Ocorreu um erro no processamento. Tente novamente mais tarde.']);
        }
    }
}
