<?php

namespace Venus\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use Venus\Http\Controllers\Controller;
use Venus\Http\Requests\Product\StoreProductRequest;
use Venus\Models\ProductCategory as Category;
use Venus\Models\Product;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id', 'DESC')->get();
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all('id', 'name');
        return view('admin.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $inputs = $request->all();

        /**
         * Refatorar essa lógica.
         * Retirar ela daqui e pôr em um local mais acessível
         */
        /** trando valores monetários para o preco*/
        $str   = str_replace('.', '', $inputs['price']); // remove o ponto
        $valor = str_replace(',', '.', $str); // troca a vírgula por ponto
        $inputs['price'] = $valor;

        if ( $request->hasFile('file') && $request->file->isValid() )
        {
            $file_name = str_slug($request->input('name'),'-');
            $file_name .= '.pdf';
            $file = $request->file->storeAs('products', $file_name);

            $product = Product::create( $inputs );
            $product->file = $file;
            $product->product_category_id = $request->input('category');
            $product->save();
        }

        return redirect()->route('products.index')->with([
            'success' => 'Produto/Ebook adicionado com sucesso!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $product->price = number_format($product->price, 2, ',', '.');
        $product->category = Category::find( $product->category->id);
        return response()->json($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all('id', 'name');
        $product = Product::findOrFail($id);
        return view('admin.products.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $inputs  = $request->except('file');

        /**
         * Refatorar essa lógica.
         * Retirar ela daqui e pôr em um local mais acessível
         */
        /** trando valores monetários para o preco */
        $str   = str_replace('.', '', $inputs['price']); // remove o ponto
        $valor = str_replace(',', '.', $str); // troca a vírgula por ponto
        $inputs['price'] = $valor;

        $product->fill($inputs);

        /**
         * Refatorar essa lógica.
         * Retirar ela daqui e pôr em um local mais acessível
         */
        /** Caso tenha haja um novo arquivo */
        if ($request->hasFile('file') && $request->file->isValid())
        {
            $file_name = explode('/', $product->file)[1];
            $file = $request->file->storeAs('products', $file_name);
            $product->file = $file;
        }

        try {
            $product->save();
            return redirect()->route('products.index')->with([
                'success' => 'Produto/Ebook atualizado'
            ]);
        } catch (\Exception $e) {
            return redirect()->route('products.index')->with([
                'error' => 'Ocorreu um erro no processo. Tente novamente em alguns instantes.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $file = $product->file;

        if ($product->delete() )
        {
            Storage::delete( $file );
            return redirect()->route('products.index')->with(['success' => 'Produto deletado com sucess']);
        } else {
            return redirect()->route('products')->with(['error' => 'Ocorreu um erro no processamento. Tente novamente mais tarde.']);
        }
    }
}
