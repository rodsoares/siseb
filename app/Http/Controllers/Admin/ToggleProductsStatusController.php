<?php

namespace Venus\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Venus\Http\Controllers\Controller;
use Venus\Models\Product;

class ToggleProductsStatusController extends Controller
{
    /**
     * @param Request $request
     * @param Product $product
     */
    public function toggle($id) {
        $product = Product::findOrFail( $id );
        $product->status = $product->status ? false : true;
        $product->save();
        return $product;
    }
}
