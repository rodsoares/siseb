<?php

namespace Venus\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Venus\Http\Controllers\Controller;
use Venus\Models\User;

class ToggleUsersStatusController extends Controller
{
    /**
     * @param $id
     * @return mixed
     */
    public function toggle($id) {
        $user = User::findOrFail( $id );
        $user->status = $user->status ? false : true;
        if ($user->save())
        {
            return redirect()->route('users.index')->with(['success' => 'Status atualizado com sucesso']);
        } else {
            return redirect()->route('users.index')->with(['error' => 'Ocorreu um erro no processamento. Tente novamente mais tarde.']);
        }
    }
}
