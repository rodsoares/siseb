<?php

namespace Venus\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Venus\Models\User;
use Kodeine\Acl\Models\Eloquent\Role;
use Venus\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id', "!=", Auth::user()->id)->get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $inputs['password'] = bcrypt($inputs['password']);

        try {
            $user = User::create($inputs);
            $user->assignRole($inputs['category']);
            return redirect()->route('users.index')->with(['success' => 'Usuário adicionado com sucesso']);
        } catch (\Exception $e) {
            return redirect()->route('users.index')->with(['error' => 'Ocorreu um erro no processo. Tente novamente em alguns instantes']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user  = User::findOrFail($id);
        $roles = Role::all();
        return view('admin.users.edit', compact('roles', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $inputs = $request->all();

        if ($inputs['password'] != null)
        {
            $inputs['password'] = bcrypt($inputs['password']);
        } else {
            $inputs['password'] = $user->password;
        }

        $user->fill($inputs);

        if( $user->save() )
        {
            $user->revokeAllRoles();
            $user->assignRole($inputs['category']);
            return redirect()->route('users.index')->with(['success' => 'Usuário adicionado com sucesso']);
        } else {
            return redirect()->route('users.index')->with(['error' => 'Ocorreu um erro no processo. Tente novamente em alguns instantes']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->revokeAllRoles();
        if ($user->delete() )
        {
            return redirect()->route('users.index')->with(['success' => 'Usuário deletado com sucesso']);
        } else {
            return redirect()->route('users.index')->with(['error' => 'Ocorreu um erro no processamento. Tente novamente mais tarde.']);
        }
    }
}
