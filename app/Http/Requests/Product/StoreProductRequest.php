<?php

namespace Venus\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'            => 'required|string|max:20',
            'name'            => 'required|string|max:191',
            'slug'            => 'required|string|unique:products,slug',
            'version'         => 'required|string|max:20',
            'size'            => 'required|string|max:20',
            'number_of_pages' => 'required|numeric',
            'file'            => 'required|file|mimes:pdf'
        ];
    }
}
