<?php

namespace Venus\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id', 'customer', 'email'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ebooks()
    {
        return $this->hasMany(OrderedProduct::class);
    }
}
