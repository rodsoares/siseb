<?php

namespace Venus\Models;

use Illuminate\Database\Eloquent\Model;

class OrderedProduct extends Model
{
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function ebook()
    {
        return $this->belongsTo(Product::class);
    }
}
