<?php

namespace Venus\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'code', 'name', 'slug', 'version', 'size', 'number_of_pages', 'price', 'description'
    ];

    /** Relationships */
    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }

    public function ordereds()
    {
        return $this->hasMany(OrderedProduct::class);
    }
}
