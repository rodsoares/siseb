<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('product_category_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('code');
            $table->string('version', 20);
            $table->string('size', 20);
            $table->integer('number_of_pages')->unsigned();
            $table->float('price');
            $table->text('description')->nullable();
            $table->string('file')->nullable();
            $table->boolean('status')->default(true)->nullable();
            $table->string('slug')->unique();
            $table->timestamps();

            $table->foreign('product_category_id')
                  ->references('id')->on('product_categories')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function(Blueprint $table) {
            $table->dropForeign(['product_category_id']);
        });
        Schema::dropIfExists('products');
    }
}
