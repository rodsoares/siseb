<?php

use Illuminate\Database\Seeder;

class ProductCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'INFORMÁTICA',
            'AUTO-AJUDA',
            'CIÊNCIA DA COMPUTAÇÃO',
            'ENGENHARIA',
            'QUÍMICA',
            'MAGIA E OCULTISMO',
            'FICÇÃO',
            'FANTASIA',
            'GASTRONOMIA',
            'ADMINISTRAÇÃO'
        ];

        foreach ($categories as $category)
        {
            DB::table('product_categories')->insert([
                'name' => $category,
                'status' => true,
                'description' => 'Mussum Ipsum, cacilds vidis litro abertis. Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis! Quem num gosta di mim que vai caçá sua turmis! Em pé sem cair, deitado sem dormir, sentado sem cochilar e fazendo pose. Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.'
            ]);
        }
    }
}
