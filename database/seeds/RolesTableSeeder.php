<?php

use Illuminate\Database\Seeder;
use Kodeine\Acl\Models\Eloquent\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $roleAdmin = $role->create([
            'name' => 'Administrador',
            'slug' => 'admin',
            'description' => 'manage administration privileges'
        ]);

        $role = new Role();
        $roleRevendedor = $role->create([
            'name' => 'Revendedor',
            'slug' => 'revendedor',
            'description' => 'Categoria revendedor de usuário'
        ]);
    }
}
