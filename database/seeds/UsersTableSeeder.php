<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username'       => 'Administrador Padrão',
            'first_name'     => 'Administrador',
            'last_name'      => 'Padrão',
            'email'          => 'admin@siseb.com.br',
            'password'       => bcrypt('siseb@2018'),
            'remember_token' => str_random(10),
        ]);

        (\Venus\Models\User::first())->assignRole('admin');
    }
}
