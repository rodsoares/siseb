$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/**
 * Obtém e exibe os dados realativos ao produto procurado
 * @param id
 */
function product_view( product_id, btn_id ) {
    var url = location.pathname + '/' + product_id;
    var btn = '#'+btn_id;

    $(btn).button('loading');

    $.get(url, function( response ) {
        console.log( response );
        $(btn).button('reset');

        /** Populando modal */
        var title   = document.querySelector('.modal-title');
        var content = document.querySelector('.modal-body')

        /** zerando possiveis valores anteriores */
        title.innerHTML = '';
        content.innerHTML = '';

        /** novos valores */
        title.innerHTML = response.name;
        var status = response.status ? 'inativo' : 'ativo';
        content.innerHTML = "" +
            "<table class='table table-bordered table-condensed'>" +
                "<thead>" +
                    "<tr class='bg-purple'>" +
                        "<th>Status</th>"+
                        "<th>Código</th>"+
                        "<th>Título</th>"+
                        "<th>Versão</th>"+
                        "<th>Categoria</th>"+
                        "<th>Preço</th>"+
                    "</tr>" +
                "</thead>"+
                "<tbody>" +
                    "<tr>" +
                        "<td>"+ status +"</td>" +
                        "<td>"+ response.code +"</td>" +
                        "<td>"+ response.name +"</td>" +
                        "<td>"+ response.version +"</td>" +
                        "<td>"+ response.category.name +"</td>" +
                        "<td>R$ "+ response.price +"</td>" +
                    "</tr>" +
                    "<tr class='bg-purple'><th colspan='6'>Descrição</th>"+
                    "<tr>" +
                        "<td colspan='6'>"+ response.description +"</td>" +
                    "</tr>" +
                    "<tr class='bg-purple'><th colspan='6'>Arquivo</th><tr>" +
                        "<td colspan='6'>"+ response.file.split('/')[1] +"</td>" +
                    "</tr>" +
                "</tbody>"+
            "</table>";

        $('#modalProductView').modal('show');
    });
}

function product_delete(product_id) {
    if ( confirm('Você tem certeza dessa ação ?') )
    {
        var url = location.pathname + '/' + product_id;
        $('#product_delete_form').attr('action', url);
        $('#product_delete_form').submit();
    }
}

function product_toggle_status(product_id) {
    var url = location.pathname + '/' + product_id + '/change-status';
    $.post(url, function(response) {
        console.log(response);
        location.reload(true);
    });
}

/** Categorias */
function product_category_delete(product_category_id) {
    if ( confirm('Você tem certeza dessa ação ?') )
    {
        var url = location.pathname + '/' + product_category_id;
        $('#product_category_delete_form').attr('action', url);
        $('#product_category_delete_form').submit();
    }
}

/** Usuários */
function user_toggle_status(user_id) {
    var url = location.pathname + '/' + user_id + '/change-status';
    $.post(url, function(response) {
        console.log(response);
        location.reload(true);
    });
}

function user_delete(user_id) {
    if ( confirm('Você tem certeza dessa ação ?') )
    {
        var url = location.pathname + '/' + user_id;
        $('#user_delete_form').attr('action', url);
        $('#user_delete_form').submit();
    }
}