@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">Formulário de cadastro</h4>
                </div>
                <div class="box-body">
                    <form action="{{ route('product-categories.update', ['id' => $category->id]) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="name">Nome da categoria</label>
                                    <input type="text" name="name" placeholder="Nome da categoria" class="form-control" id="name" value="{{ $category->name }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label>Descrição</label>
                                    <textarea name="description" class="form-control">{{ $category->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="button" class="btn btn-danger" onclick="location.href='{{ route('product-categories.index') }}';">
                                <i class="fa fa fa-fw fa-arrow-left"></i>Retornar</button>
                            <button type="submit" class="btn btn-primary">Atualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <!-- Include ckEditor library -->
    <script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/classic/ckeditor.js"></script>
    <!-- Initialize Quill editor -->
    <script>
        $(document).ready(function () {
            ClassicEditor
                .create( document.querySelector( 'textarea' ) )
                .catch( error => {
                console.error( error );
        });
        });
    </script>
@stop