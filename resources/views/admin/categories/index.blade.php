@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="box with-border">
                <div class="box-header">
                    <h3 class="box-title">
                        Listagem geral de Categorias &nbsp;
                        <a role="button" class="btn btn-xs btn-primary" href="{{ route('product-categories.create') }}">
                            <i class="fa fa-fw fa-plus"></i>
                            Adicionar Categoria
                        </a>
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->name }}</td>
                                <td class="text-right">
                                    <div class="btn-group btn-group-xs" role="group" aria-label="...">
                                        <a role="button" class="btn btn-warning" href="{{ route('product-categories.edit', ['id' => $category->id]) }}">
                                            <i class="fa fa-fw fa-edit"></i> atualizar
                                        </a>
                                        <button type="button" class="btn btn-danger"
                                                onclick="product_category_delete({{$category->id}})">
                                            <i class="fa fa-fw fa-trash-o"></i> deletar
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Formulário de exclusão geral -->
    <form id="product_category_delete_form" method="POST">
        {{ csrf_field() }}
        {{ method_field("DELETE") }}
    </form>
@stop

@section('js')
    <script>
        $('table').DataTable();
    </script>
@stop