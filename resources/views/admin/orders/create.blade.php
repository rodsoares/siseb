@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">Formulário de cadastro</h4>
                </div>
                <div class="box-body">
                    <form enctype="multipart/form-data" action="{{ route('orders.store') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <div class="row">
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <div class="form-group">
                                    <label for="customer">Nome do Cliente</label>
                                    <input type="text" name="customer" placeholder="Nome do cliente" class="form-control" id="customer" required>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="email">Email para envio</label>
                                    <input type="text" name="email" placeholder="Email para envio" class="form-control" id="email" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label>Ebooks</label>
                                    <select name="ebooks[]" class="form-control" id="ebooks" multiple required>
                                        @foreach( $products as $ebook )
                                            <option value="{{ $ebook->id }}">{{ $ebook->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="button" class="btn btn-danger" onclick="location.href='{{ route('product-categories.index') }}';">
                                <i class="fa fa fa-fw fa-arrow-left"></i>Retornar</button>
                            <button type="submit" class="btn btn-primary">Adicionar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <!-- Include ckEditor library -->
    <script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/classic/ckeditor.js"></script>
    <!-- Initialize Quill editor -->
    <script>
        $(document).ready(function () {
            ClassicEditor
                .create( document.querySelector( 'textarea' ) )
                .catch( error => {
                console.error( error );
        });
            $('#ebooks').select2();
        });
    </script>
@stop