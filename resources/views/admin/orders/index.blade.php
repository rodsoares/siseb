@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="box with-border">
                <div class="box-header">
                    <h3 class="box-title">
                        Listagem geral de Pedidos
                        <a role="button" class="btn btn-xs btn-primary" href="{{ route('orders.create') }}">
                            <i class="fa fa-fw fa-plus"></i>
                            Novo
                        </a>
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Status</th>
                            <th>Cliente</th>
                            <th>Email</th>
                            <th>Preço</th>
                            <th>Processamento</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>
                                    @if( $order->status == 'enviado' )
                                        <span class="label label-success">
                                            <i class="fa fa-fw fa-check"></i> enviado
                                        </span>
                                    @elseif( $order->status == 'reenviado' )
                                        <span class="label label-warning">
                                            <i class="fa fa-fw fa-reorder"></i> renviado
                                        </span>
                                    @elseif( $order->status == 'reembolsado' )
                                        <span class="label label-danger">
                                            <i class="fa fa-fw fa-close"></i> reembolsado
                                        </span>
                                    @endif
                                </td>
                                <td>{{ $order->customer }}</td>
                                <td>{{ $order->email }}</td>
                                <td>R$ {{ number_format($order->total, 2, ',', '.') }}</td>
                                <td>{{ $order->created_at->format('d/m/Y H:i:s') }}</td>
                                <td class="text-right">
                                    @if( $order->status )
                                        <button type="btn" class="btn btn-danger btn-xs"
                                                onclick="order_toggle_status({{$order->id}})">desativar</button>
                                    @endif

                                    <div class="btn-group btn-group-xs" role="group" aria-label="...">
                                        <button type="button" class="btn btn-primary" id="order_{{ $order->id }}"
                                                onclick="order_view({{$order->id}}, 'order_{{$order->id}}')"
                                                data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Carregando...">
                                            <i class="fa fa-fw fa-eye"></i> exibir
                                        </button>
                                        <a role="button" class="btn btn-warning" href="{{ route('orders.edit', ['id' => $order->id]) }}">
                                            <i class="fa fa-fw fa-edit"></i> atualizar
                                        </a>
                                        <button type="button" class="btn btn-danger"
                                                onclick="order_delete({{$order->id}})">
                                            <i class="fa fa-fw fa-trash-o"></i> deletar
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalOrderView" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Formulário de exclusão geral -->
    <form id="order_delete_form" method="POST">
        {{ csrf_field() }}
        {{ method_field("DELETE") }}
    </form>
@stop

@section('js')
    <script>
        $('table').DataTable();
    </script>
@stop