@extends('layouts.base')

@section('breadcrumb')
    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="">Administração</a></li>
        <li class="breadcrumb-item">Produtos</li>
        <li class="breadcrumb-item active">Editar <i>{{ $product->name }}</i></li>
    </ul>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">Formulário de edição</h4>
                </div>
                <div class="box-body">
                    <form enctype="multipart/form-data" action="{{ route('products.update', ['id' => $product->id]) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-sm-12 col-md-2 col-lg-2">
                                <div class="form-group">
                                    <label for="code">Código</label>
                                    <input type="text" name="code" placeholder="Código do produto" class="form-control" id="code" value="{{ $product->code }}">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="name">Título / Nome</label>
                                    <input type="text" name="name" placeholder="Título/Nome do produto" class="form-control" id="name" data-slugify="#slug" value="{{ $product->name }}">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label for="name">Slug</label>
                                    <input type="text" name="slug" class="form-control" id="slug" value="{{ $product->slug }}">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label for="code">Categoria</label>
                                    <select name="category" class="form-control" id="product_category">
                                        @foreach( $categories as $category )
                                            <option value="{{ $category->id }}" @if( $product->category->id == $category->id) selected @endif>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label for="version">Versão</label>
                                    <input type="text" name="version" placeholder="Versão do produto" class="form-control" id="version" value="{{ $product->version }}">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label for="size">Dimensões / Tamanho</label>
                                    <input type="size" name="size" placeholder="Dimensões e/ou tamanho do produto" class="form-control" id="size" value="{{ $product->size }}">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label for="number_of_pages">Quantidade de Páginas</label>
                                    <input type="number" min="0" name="number_of_pages" class="form-control" id="number_of_pages" value="{{ $product->number_of_pages }}">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label for="price">Preço (R$)</label>
                                    <input type="text" name="price" placeholder="R$ 00,00" class="form-control" id="price" value="{{ number_format($product->price, 2, ',', '.') }}"
                                           data-mask="#.##0,00" data-mask-reverse="true">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Descrição</label>
                            <textarea name="description" class="form-control" rows="5">{{ $product->description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="file" >Arquivo / Ebook</label>
                            <input type="file" name="file" class="form-control-file">
                            <p class="help-block">Extensões permitidas: PDF</p>
                        </div>

                        <div class="form-group text-right">
                            <button type="button" class="btn btn-danger" onclick="location.href='{{ route('products.index') }}';">
                                <i class="fa fa fa-fw fa-arrow-left"></i>Retornar</button>
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-fw fa-check"></i>Atualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <!-- Include ckEditor library -->
    <script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/classic/ckeditor.js"></script>
    <!-- Initialize Quill editor -->
    <script>
        $(document).ready(function () {
            ClassicEditor
                .create( document.querySelector( 'textarea' ) )
                .catch( error => {
                console.error( error );
        });
            $('select').select2();
        });
    </script>
@stop