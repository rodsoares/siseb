@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="box with-border">
                <div class="box-header">
                    <h3 class="box-title">
                        Listagem geral dos Ebooks &nbsp;
                        <a role="button" class="btn btn-xs btn-primary" href="{{ route('products.create') }}">
                            <i class="fa fa-fw fa-plus"></i>
                            Adicionar
                        </a>
                        <a role="button" class="btn btn-xs btn-warning" href="{{ route('product-categories.create') }}">
                            <i class="fa fa-fw fa-plus"></i>
                            Adicionar Categoria
                        </a>
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Status</th>
                            <th>Código</th>
                            <th>Título</th>
                            <th>Versão</th>
                            <th>Categoria</th>
                            <th>Preço</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>
                                    @if( $product->status )
                                        <span class="label label-success">
                                            <i class="fa fa-fw fa-check-circle-o"></i> ativo
                                        </span>
                                    @else
                                        <span class="label label-danger">
                                            <i class="fa fa-fw fa-close"></i> inativo
                                        </span>
                                    @endif
                                </td>
                                <td>{{ $product->code }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->version }}</td>
                                <td>{{ $product->category->name }}</td>
                                <td>R$ {{ number_format($product->price, 2, ',', '.') }}</td>
                                <td class="text-right">
                                    @if(in_array('admin', Auth::user()->getRoles()) )
                                        <a role="button" class="btn btn-xs btn-default"
                                           href="{{ route('products.download', ['slug'=> $product->slug]) }}">
                                            <i class="fa fa-fw fa-download"></i> download
                                        </a>
                                    @endif

                                    @if( $product->status )
                                        <button type="btn" class="btn btn-danger btn-xs"
                                                onclick="product_toggle_status({{$product->id}})">desativar</button>
                                    @else
                                        <button type="btn" class="btn btn-success btn-xs"
                                                onclick="product_toggle_status({{$product->id}})">Ativar
                                        </button>
                                    @endif

                                    <div class="btn-group btn-group-xs" role="group" aria-label="...">
                                        <button type="button" class="btn btn-primary" id="product_{{ $product->id }}"
                                                onclick="product_view({{$product->id}}, 'product_{{$product->id}}')"
                                                data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Carregando...">
                                            <i class="fa fa-fw fa-eye"></i> exibir
                                        </button>
                                        <a role="button" class="btn btn-warning" href="{{ route('products.edit', ['id' => $product->id]) }}">
                                            <i class="fa fa-fw fa-edit"></i> atualizar
                                        </a>
                                        <button type="button" class="btn btn-danger"
                                            onclick="product_delete({{$product->id}})">
                                            <i class="fa fa-fw fa-trash-o"></i> deletar
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalProductView" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Formulário de exclusão geral -->
    <form id="product_delete_form" method="POST">
        {{ csrf_field() }}
        {{ method_field("DELETE") }}
    </form>
@stop

@section('js')
    <script>
        $('table').DataTable();
    </script>
@stop