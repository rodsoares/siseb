@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">Formulário de cadastro</h4>
                </div>
                <div class="box-body">
                    <form enctype="multipart/form-data" action="{{ route('users.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label for="username">Nome</label>
                                    <input type="text" name="username" placeholder="Nome do Usuário" class="form-control" id="username" required>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" placeholder="usuario@email.com" class="form-control" id="email" required>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label for="category">Categoria</label>
                                    <select name="category" class="form-control" id="user_category" required>
                                        @foreach( $roles as $role )
                                            <option value="{{ $role->slug }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label for="password">Senha</label>
                                    <input type="password" name="password" placeholder="Senha" class="form-control" id="password" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    Informações de Revendedor
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-7 col-lg-7">
                                                        <div class="form-group">
                                                            <label for="address">Endereço</label>
                                                            <input type="text" name="address" placeholder="Endereço" class="form-control" id="address">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-2 col-lg-2">
                                                        <div class="form-group">
                                                            <label for="cep">Cep</label>
                                                            <input type="text" name="cep" placeholder="00000-000" class="form-control" id="cep">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-2 col-lg-2">
                                                        <div class="form-group">
                                                            <label for="city">Cidade</label>
                                                            <input type="text" name="city" placeholder="Cidade" class="form-control" id="city">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-1 col-lg-1">
                                                        <div class="form-group">
                                                            <label for="region">Estado</label>
                                                            <input type="text" name="region" placeholder="XX" class="form-control" id="region">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4 col-lg-4">
                                                        <div class="form-group">
                                                            <label for="cpf">Cpf</label>
                                                            <input type="text" name="cpf" placeholder="00000-000" class="form-control" id="cpf"
                                                                   data-mask="000.000.000-00" data-mask-reverse="true">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-4 col-lg-4">
                                                        <div class="form-group">
                                                            <label for="cnpj">Cnpj</label>
                                                            <input type="text" name="cnpj" placeholder="00000-000" class="form-control" id="cnpj"
                                                                   data-mask="00.000.000/0000-00" data-mask-reverse="true">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-4 col-lg-4">
                                                        <div class="form-group">
                                                            <label for="bank_account">Conta bancária</label>
                                                            <input type="text" name="bank_account" placeholder="BB 0000-0 00000-0" class="form-control" id="bank_account">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-9 col-lg-9">
                                                        <div class="form-group">
                                                            <label for="websites">Websites</label>
                                                            <input type="text" name="websites"
                                                                   placeholder="meusite.com.br;meuoutrosite.com.br;meuoutrooutrosite.com.br"
                                                                   class="form-control"
                                                                   id="websites">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-3 col-lg-3">
                                                        <div class="form-group">
                                                            <label for="phone">Celular/Whatsapp</label>
                                                            <input type="text" name="phone" placeholder="(xx)xxxxx-xxxx" class="form-control" id="phone"
                                                                   data-mask="(00)00000-0000">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group text-right">
                            <button type="button" class="btn btn-danger" onclick="location.href='{{ route('users.index') }}';">
                                <i class="fa fa fa-fw fa-arrow-left"></i>Retornar</button>
                            <button type="submit" class="btn btn-primary">Adicionar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop