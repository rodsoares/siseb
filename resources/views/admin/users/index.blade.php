@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="box with-border">
                <div class="box-header">
                    <h3 class="box-title">
                        Listagem geral dos Usuários do Sistema
                        <a role="button" class="btn btn-xs btn-primary" href="{{ route('users.create') }}">
                            <i class="fa fa-fw fa-plus"></i>
                            Adicionar
                        </a>
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Status</th>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Tipo</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    @if( $user->status )
                                        <span class="label label-success">
                                            <i class="fa fa-fw fa-check-circle-o"></i> ativo
                                        </span>
                                    @else
                                        <span class="label label-danger">
                                            <i class="fa fa-fw fa-close"></i> inativo
                                        </span>
                                    @endif
                                </td>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @foreach( $user->getRoles() as $role )
                                        {{ $role }}
                                    @endforeach
                                </td>
                                <td class="text-right">

                                    @if( $user->status )
                                        <button type="btn" class="btn btn-danger btn-xs"
                                                onclick="user_toggle_status({{$user->id}})">desativar</button>
                                    @else
                                        <button type="btn" class="btn btn-success btn-xs"
                                                onclick="user_toggle_status({{$user->id}})">Ativar
                                        </button>
                                    @endif

                                    <div class="btn-group btn-group-xs" role="group" aria-label="...">
                                        <a role="button" class="btn btn-warning" href="{{ route('users.edit', ['id' => $user->id]) }}">
                                            <i class="fa fa-fw fa-edit"></i> atualizar
                                        </a>
                                        <button type="button" class="btn btn-danger"
                                            onclick="user_delete({{$user->id}})">
                                            <i class="fa fa-fw fa-trash-o"></i> deletar
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Formulário de exclusão geral -->
    <form id="user_delete_form" method="POST">
        {{ csrf_field() }}
        {{ method_field("DELETE") }}
    </form>
@stop

@section('js')
    <script>
        $('table').DataTable();
    </script>
@stop