<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('download/{product_slug}','Admin\DownloadsController@download')->name('public.download');

Route::prefix('admin')->middleware(['auth', 'acl', 'redirect.status'])->group( function () {
    Route::namespace('Admin')->group( function() {

        /** Dashboard */
        Route::get('/', 'DashboardController@index')->name('admin.dashboard');

        /** Pedidos */
        Route::resource('orders', 'OrdersController');

        /** ROTAS ADMINISTRATIVAS PROTEGIDAS */
        Route::group(['is' => 'admin'], function () {
            /** Rotas Categorias de Produtos */
            Route::resource('product-categories', 'ProductCategoriesController');

            /** Rotas Produtos */
            Route::resource('products', 'ProductsController');
            Route::post('products/{product}/change-status', 'ToggleProductsStatusController@toggle')->name('products.toggle-status');
            Route::get('products/{slug}/download', 'DownloadsController@download')->name('products.download');

            /** Rotas Usuários */
            Route::resource('users', 'UsersController');
            Route::post('users/{user}/change-status', 'ToggleUsersStatusController@toggle')->name('users.toggle-status');

        });
    });
});
